*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.

Library     Selenium2Library
Library     BuiltIn
Library     Collections


Resource    resource.robot

*** Variables ***
${SERVER}         http://fetchrobotics.com
${BROWSER}        chrome
${DELAY}           0.5

*** Keywords ***
Open Browser To Fetch homepage
    Open Browser    ${SERVER}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Fetch homepage should come online
    Title Should Be     Autonomous mobile robots for warehouse and commercial environments - Fetch Robotics


Parse lines and resolve links
    [Arguments]    ${LIST}
    ${RESULT}=    Set Variable    0
    : FOR    ${LINE}    IN    @{LIST}
    \    Log    ${LINE}
    \    @{COLUMNS}=    Split String    ${LINE}    separator=,
    \    ${URL}=    Get From List    ${COLUMNS}    0
    \    ${TITLE}=    Get From List    ${COLUMNS}    1
    \    Log    ${URL}
    \    Log    ${TITLE}
    \    Go To   ${URL}
    \    Title Should Be     ${TITLE}
#    \    Sleep 4

