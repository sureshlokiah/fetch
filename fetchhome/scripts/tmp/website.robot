*** Settings ***
Documentation   Suite to test the homesite
Library         Selenium2Library

# Resource  ../resources/keywords/testlib.robot
# Resource  ../resources/keywords/tools/tools.robot

*** Test Cases ***



#Test SiteUp
#    [Tags]    DEBUG
#    Provided precondition
#    When Launching the fetch homepage
    # Get timestamp
    # Then check homepage is loaded

Test Site is Up
    [Tags]    DEBUG
    #Given Open Browser to Portal Login Page
    When Launching the google homepage
    #Then Summary Tab is shown with SecurityOrb
    Close Browser

#*** Keywords ***
#Provided precondition
#  Setup system under test

*** Keywords ***
Launching the google homepage
  Open Browser    http://google.com
  maximize browser window
  #set selenium speed  ${DELAY}
