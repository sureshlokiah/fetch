*** Settings ***
Documentation    Suite description
Library           OperatingSystem
Library           String
Library           Collections

Resource    resource.robot

# Test Teardown     Close Browser

*** Test Cases ***
# Test Case 1
#### POSITIVE CONDITION
Test Fetch website is Up
#### 1. Launch browser to the Fetch homepage
#### 2. Check the title matches the page(landing Url) title.
#### ==> PASS
    When Open Browser To Fetch homepage
    Then Fetch homepage should come online

# Test Case 2
#### POSITIVE CONDITION
Test Menu links are resolving
#### 1. Read the file data (16 lines) into a list;
#### 2. Parse each item having urls, title
#### 3. Check the title matches the page(landing Url) title.
#### ==> PASS
    [Tags]    file-reading
    ${FILE_CONTENT}=   Get File    ../../data/landing_title.txt
    Log    File Content: ${FILE_CONTENT}
    @{LINES}=   Split to Lines  ${FILE_CONTENT}
    Log    ${LINES}
    Parse lines and resolve links   ${LINES}
    Close Browser

# Test Case 3
#### ------ NEGATIVE CONDITION
Test Invalid landing page
#### 1. Launch browser to the Fetch homepage
#### 2. Check for an INVALID title for the Page
#### ==> FAIL
    Open Browser    ${SERVER}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Title Should Be     INVALID TITLE - Fetch Robotics
    Close Browser
