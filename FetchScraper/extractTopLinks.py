#!/usr/local/bin/python3

from urllib.request import Request, urlopen

import os
import time
from bs4 import BeautifulSoup
# import re


## Get the page from web;
## Use User-Agent to trick the server.
def getpage(url):
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    content = urlopen(req).read()

    return(content)


## Save the url pages to a file.
def savepage(content, fname):
    fhandle = open(fname, 'w')
    fhandle.write(str(content))
    fhandle.close()

## Process the saved url page to extract 'title'
def processpage(fname):
    fhandle = open(fname, "r")
    webpage = fhandle.read()

    soup = BeautifulSoup(webpage)
    title = soup.find('title').text
    # print(title)

    return(title)

## Save the url,title into a landing_title.txt
def store_url_title(url, title):
    fname = '../data/landing_title.txt'
    fmode = 'a'
    if os.path.isfile('fname'):
        mode = 'w'
    with open(fname, fmode) as f:
        f.write(url+','+ title + '\n')


### Main
seqno = 1;
flanding = open("./landing_pages.txt", 'r')

for line in (flanding):
    url = line.strip()
    # if seqno == 3: break
    content= getpage(url)
    fname = './pages/fetch_' + str(seqno) + '.html'

    savepage(content, fname)
    title = processpage(fname)
    store_url_title(url, title)

    time.sleep(4)
    seqno = int(seqno) + 1

flanding.close()




#fhandle = open("./fetch.html", "r")
#webpage = fhandle.read()

#re_menuitem = re.compile("menu-item-\d+")
# lst = soup.find_all('li', {'id': 'menu-item-2000'})
#lst = soup.find_all('li', {'id': re_menuitem})

#for line in lst:
#    print(str(line),"\n")
# print(lst.join("\n"))
#print(str(lst))
#/html/head/title/text()

#Fetch Robots at DHL partner Wärtsilä from Fetch Robotics on Vimeo

